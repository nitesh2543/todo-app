package com.signeasy.todolist.mvvm.view.activity;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.orm.SugarApp;
import com.orm.SugarConfig;
import com.orm.SugarDb;
import com.signeasy.todolist.mvvm.view.fragment.AddTaskFragment;
import com.signeasy.todolist.R;
import com.signeasy.todolist.mvvm.view.fragment.TaskListingFragment;
import com.signeasy.todolist.util.Constants;
import com.signeasy.todolist.util.FragmentHelper;

public class MainActivity extends BaseActivity implements Constants.TaskListener {

    private final int REQUEST_PERMISSIONS = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_main);
        SugarApp.getSugarContext().onCreate();
        initUI();
    }

    private void initUI() {
        if (!isStoragePermissionGranted())
            requestPermission();
        FragmentHelper.addFragment(this, R.id.container1, new AddTaskFragment());
        FragmentHelper.addFragment(this, R.id.container2, new TaskListingFragment());
    }

    @Override
    public void onTaskAdded() {
        Fragment fragment = FragmentHelper.getFragment(this, R.id.container2);
        if (fragment instanceof TaskListingFragment)
            ((TaskListingFragment) fragment).updateList();
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS)
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
    }
}
