package com.signeasy.todolist.mvvm.view.fragment;


import android.app.Fragment;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.signeasy.todolist.R;
import com.signeasy.todolist.databinding.FragmentTaskListingBinding;
import com.signeasy.todolist.mvvm.model.Task;
import com.signeasy.todolist.mvvm.view.adapter.TaskListingAdapter;
import com.signeasy.todolist.util.RecyclerViewEvents;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TaskListingFragment extends BaseFragment implements RecyclerViewEvents.Listener<Task>,
        SwipeRefreshLayout.OnRefreshListener {

    private FragmentTaskListingBinding binding;
    private List<Task> taskList;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task_listing, container, false);
        binding.recyclerView.setAdapter(new TaskListingAdapter(inflater, taskList, this));
        binding.swipeRefreshLayout.setOnRefreshListener(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateList();
    }

    public void updateList() {
        binding.swipeRefreshLayout.setRefreshing(true);
        List<Task> tasks = Task.listAll(Task.class);
        this.taskList.clear();
        this.taskList.addAll(tasks);
        binding.recyclerView.getAdapter().notifyDataSetChanged();
        binding.swipeRefreshLayout.setRefreshing(false);
        binding.emptyView.setVisibility(taskList.size() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onItemClick(Task item, View v, int position) {
        Toast.makeText(getActivity(), item.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        updateList();
    }
}
