package com.signeasy.todolist.mvvm.model;

import com.orm.SugarRecord;

public class Task extends SugarRecord {

    private String title;
    private String category;

    public Task() {
    }

    public Task(String title, String category) {
        this.title = title;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }
}
