package com.signeasy.todolist.mvvm.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.signeasy.todolist.R;
import com.signeasy.todolist.BR;
import com.signeasy.todolist.databinding.AdapterTaskListingBinding;
import com.signeasy.todolist.mvvm.model.Task;
import com.signeasy.todolist.util.RecyclerViewEvents;
import com.signeasy.todolist.util.RecyclerViewHolder;
import com.signeasy.todolist.util.RecyclerViewOnItemClickHandler;

import java.util.List;

public class TaskListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final List<Task> list;
    private final RecyclerViewEvents.Listener<Task> listener;

    public TaskListingAdapter(LayoutInflater layoutInflater, List<Task> list,
                              RecyclerViewEvents.Listener<Task> listener) {
        this.layoutInflater = layoutInflater;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            default:
                AdapterTaskListingBinding binding = DataBindingUtil.inflate(layoutInflater,
                        R.layout.adapter_task_listing, parent, false);
                return new RecyclerViewHolder<>(binding);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RecyclerViewHolder) {
            Task item = list.get(position);
            RecyclerViewHolder photoViewHolder = (RecyclerViewHolder) holder;
            photoViewHolder.getBinding().setVariable(BR.clickListener,
                    new RecyclerViewOnItemClickHandler<>(item, position, listener));
            photoViewHolder.getBinding().setVariable(BR.task, item);
            photoViewHolder.getBinding().executePendingBindings();
        }

    }
}
