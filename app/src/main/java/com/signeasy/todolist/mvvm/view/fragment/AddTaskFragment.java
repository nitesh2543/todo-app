package com.signeasy.todolist.mvvm.view.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.signeasy.todolist.R;
import com.signeasy.todolist.databinding.FragmentAddTaskBinding;
import com.signeasy.todolist.mvvm.model.Task;
import com.signeasy.todolist.util.Constants;
import com.signeasy.todolist.util.FragmentHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddTaskFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private FragmentAddTaskBinding binding;
    private List<String> categories;
    private Constants.TaskListener listener;
    private String title;
    private String category;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (Constants.TaskListener) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (Constants.TaskListener) activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createCategoryList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_task, container, false);
        binding.spinner.setOnItemSelectedListener(this);
        binding.addTask.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        category = adapterView.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_task:
                addTask();
                listener.onTaskAdded();
                break;
        }
    }

    private void addTask() {
        title = binding.taskTitle.getText().toString();
        if (TextUtils.isEmpty(title) || title == null) {
            Toast.makeText(getActivity(), R.string.err_title, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(category) || category == null) {
            Toast.makeText(getActivity(), R.string.err_category, Toast.LENGTH_SHORT).show();
            return;
        }
        Task task = new Task(title, category);
        task.save();
        binding.taskTitle.setText("");
        hideKeyboard(getActivity());
    }

    private void createCategoryList(){
        /*Fake data for category*/
        categories = new ArrayList();
        categories.add("Office");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");
    }

    private static void hideKeyboard(Context ctx) {
        View view = ((Activity) ctx).getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
